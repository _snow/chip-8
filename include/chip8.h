#ifndef CHIP8_H
#define CHIP8_H

#define MEM_SIZE 0x1000 // 1 4KB page for C8, SCHIP8 and CHIP48 can be larger but I'm not supporting those
#define REG_SIZE 0x10
#define STACK_SIZE 0x10

#define MEM_CHIP_START 0x0 // Interpreter occupies first 512 bytes
#define MEM_PROG_START 0x200 // Default program mapping
#define PROG_SIZE 0xD00
#define MEM_FONT_START (MEM_PROG_START + PROG_SIZE) // Just put it here lol

typedef struct chip8_t {
    uint8_t mem[MEM_SIZE];
    uint8_t V[REG_SIZE]; // VF is a flags register
    uint16_t I; // Register usually used for mem addresses
    uint16_t stack[STACK_SIZE];
    uint16_t sound_timer;
    uint16_t delay_timer;
    uint16_t pc;
    uint8_t sp;
} chip8_t;

void bin_to_bcd(uint8_t bin, uint8_t* bcd);

chip8_t* chip8_init(void);
long chip8_load(chip8_t* chip8, char* fname);
void chip8_loop(SDL_Renderer *ren);
void execute(chip8_t* chip8, SDL_Renderer *ren, uint16_t opcode);

#endif // CHIP8_H
