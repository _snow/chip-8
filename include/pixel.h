#ifndef PIXEL_H
#define PIXEL_H

#define NATIVE_WIDTH 64
#define NATIVE_HEIGHT 32
#define SCALE_FACTOR 12
#define WINDOW_WIDTH (NATIVE_WIDTH*SCALE_FACTOR)
#define WINDOW_HEIGHT (NATIVE_HEIGHT*SCALE_FACTOR)

#define FILL_COLOR 200,200,200,255
#define CLEAR_COLOR 20,20,20,255

#define TOTAL_PIXELS (NATIVE_WIDTH*NATIVE_HEIGHT)
#define TOTAL_PIXEL_BYTES (TOTAL_PIXELS/8)

// in each byte, the pixels are stored backwards
// least significant bit is the pixel left-most
uint8_t pixels[(NATIVE_WIDTH*NATIVE_HEIGHT)/8];

void set_pixel(SDL_Renderer *ren, uint16_t x, uint16_t y, uint8_t val);
uint8_t get_pixel(uint16_t x, uint16_t y);

void clear_display(SDL_Renderer* ren);
uint8_t need_screen_update(void);
void screen_update_done(void);
uint8_t display_sprite(chip8_t* chip8, SDL_Renderer *ren, uint16_t x, uint16_t y, uint16_t addr, uint8_t height);

#endif // PIXEL_H
