#ifndef FONT_H
#define FONT_H

#define FONT_WIDTH 4
#define FONT_HEIGHT 5

extern const uint8_t font[16][FONT_HEIGHT];

#endif // FONT_H
