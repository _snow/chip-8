#ifndef KEYBOARD_H
#define KEYBOARD_H

#define KEY_ESC 0x1b

const uint8_t key_map_keycodes[16];

uint8_t key_map[16];

extern uint8_t key_syn;
extern uint8_t key_ack;
extern uint8_t pressed_key;

void key_down_handler(SDL_KeyboardEvent e);
void key_up_handler(SDL_KeyboardEvent e);
uint8_t key_status(unsigned char key);
/*

ORIGINAL CHIP-8 KEYBOARD
+---+---+---+---+
| 1 | 2 | 3 | C |
+---+---+---+---+
| 4 | 5 | 6 | D |
+---+---+---+---+
| 7 | 8 | 9 | E |
+---+---+---+---+
| A | 0 | B | F |
+---+---+---+---+


NEW CHIP-8 KEYBOARD
+---+---+---+---+
| 1 | 2 | 3 | 4 |
+---+---+---+---+
| Q | W | E | R |
+---+---+---+---+
| A | S | D | F |
+---+---+---+---+
| Z | X | C | V |
+---+---+---+---+
*/

#endif // KEYBOARD_H 
