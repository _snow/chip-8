#include <stdio.h>
#include <stdlib.h>

#include <SDL2/SDL.h>

#include "../include/chip8.h"
#include "../include/pixel.h"

int main(int argc, char* argv[]) {
    if (argc < 2) {
        fprintf(stderr, "usage: chip8 rom_file\n");
        return -1;
    }

    SDL_Window *win = NULL;
    SDL_Renderer *ren = NULL;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Couldn't initialize SDL: %s", SDL_GetError());
        return 3;
    }

    win = SDL_CreateWindow("C8EMU", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);
    if (win == NULL) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Couldn't create window: %s", SDL_GetError());
        return 3;
    }

    ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED);
    if (ren == NULL) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Couldn't create renderer: %s", SDL_GetError());
        return 3;
    }

    SDL_SetRenderDrawColor(ren, CLEAR_COLOR);
    SDL_RenderClear(ren);
    SDL_RenderPresent(ren);

    chip8_t* chip8 = chip8_init();
    chip8_load(chip8, argv[1]);
    chip8_loop(ren);

    SDL_DestroyWindow(win);
    SDL_DestroyRenderer(ren);
    return 0;
}

