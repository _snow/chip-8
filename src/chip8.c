#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <SDL2/SDL.h>

#include "../include/chip8.h"
#include "../include/keyboard.h"
#include "../include/pixel.h"
#include "../include/font.h"

chip8_t* chip8 = NULL;
size_t fsize = 0;

void bin_to_bcd(uint8_t bin, uint8_t* bcd) {
    // Ones in idx 2, hundreds in idx 0
    memset(bcd, 0, 3);
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 3; j++) {
            if (bcd[j] >= 5)
                bcd[j] += 3;
        }
        for (int j = 0; j < 3; j++) {
            bcd[j] <<= 1;
            if (j < 2)
                bcd[j] = (bcd[j] & 0xE) | ((bcd[j+1] >> 3) & 0x1);
        }
        bcd[2] = (bcd[2] & 0xE) | ((bin >> 7) & 0x1);
        bin <<= 1;
    }
}

chip8_t* chip8_init() {
    chip8 = (chip8_t*)malloc(sizeof(chip8_t));
    chip8->sound_timer = 0;
    chip8->delay_timer = 0;
    chip8->pc = MEM_PROG_START;
    chip8->sp = 0;
    memcpy(chip8->mem + MEM_FONT_START, font, sizeof(font));
    srand(time(0));

    printf("Init complete\n");
    return chip8;
}

// Return bytes read
long chip8_load(chip8_t* chip8, char* fname) {
    FILE* fp = fopen(fname, "r");

    size_t bytes_read = fread(chip8->mem + MEM_PROG_START, 1, PROG_SIZE, fp);
    fclose(fp);

    printf("Bytes loaded: %ld\n", bytes_read);

    printf("Load complete\n");
    fsize = (bytes_read < PROG_SIZE) ? bytes_read : PROG_SIZE;
    return fsize;
}

void chip8_loop(SDL_Renderer *ren) {
    uint8_t quit = 0;
    unsigned int last_operation_time, last_frame_time, current_time;
    last_operation_time = last_frame_time = 0;
    SDL_Event e;

    while(!quit) {
        if ((chip8->pc > MEM_PROG_START) && ((uint16_t)(chip8->pc - MEM_PROG_START) >= fsize)) {
            quit = 1;
            continue;
        }

        if (SDL_PollEvent(&e)) {
            if (e.type == SDL_QUIT)
                break;
            else if (e.type == SDL_KEYUP) {
                if (e.key.keysym.sym == SDLK_ESCAPE)
                    break;
                key_up_handler(e.key);
            } else if (e.type == SDL_KEYDOWN) {
                key_down_handler(e.key);
            }
        }

        current_time = SDL_GetTicks();
        // 8 opcodes per frame, 480 opcodes per second
        if (current_time > (last_operation_time+2)) {
            uint16_t opcode = (chip8->mem[chip8->pc] << 8) | (chip8->mem[chip8->pc+1] & 0xFF);
            chip8->pc += 2;
        
            execute(chip8, ren, opcode);

            last_operation_time = current_time;
        } 

        // 60 FPS
        if (current_time > (last_frame_time+16)) {
            chip8->delay_timer = chip8->delay_timer == 0 ? 0 : (chip8->delay_timer - 1);
            chip8->sound_timer = chip8->sound_timer == 0 ? 0 : (chip8->sound_timer - 1);
            if (need_screen_update())
                SDL_RenderPresent(ren);
            last_frame_time = current_time;
        }
        nanosleep((const struct timespec[]){{0, 50000L}}, NULL);
    }
    return;
}

void execute(chip8_t* chip8, SDL_Renderer *ren, uint16_t opcode) {
    uint8_t msnibble = opcode >> 12; // most significant nibble
    uint8_t lsnibble = opcode & 0xF; // least significant nibble
    uint8_t vx = (opcode & 0xF00) >> 8; 
    uint8_t vy = (opcode & 0xF0) >> 4;
    uint8_t lo_byte = (opcode & 0xFF);
    uint16_t sum;
    // uint8_t hi_byte = (opcode & 0xFF00) >> 8;

    switch (msnibble) {
        case 0x0: // Ignore opcode ONNN for now, most ROMs don't use it
            if (lsnibble == 0xE) {
                chip8->pc = chip8->stack[chip8->sp];
                chip8->sp--;
            } else if (!lsnibble) {
                clear_display(ren);
            }
            break;
        case 0x1:
            chip8->pc = opcode & 0xFFF;
            break;
        case 0x2:
            chip8->sp++;
            chip8->stack[chip8->sp] = chip8->pc;
            chip8->pc = opcode & 0xFFF;
            break;
        case 0x3:
            chip8->pc += chip8->V[vx] == lo_byte ? 2 : 0;
            break;
        case 0x4:
            chip8->pc += chip8->V[vx] != lo_byte ? 2 : 0;
            break;
        case 0x5:
            chip8->pc += chip8->V[vx] == chip8->V[vy] ? 2 : 0;
            break;
        case 0x6:
            chip8->V[vx] = lo_byte;
            break;
        case 0x7:
            chip8->V[vx] += lo_byte;
            break;
        case 0x8:
            switch (lsnibble) {
                case 0x0:
                    chip8->V[vx] = chip8->V[vy];
                    break;
                case 0x1:
                    chip8->V[vx] |= chip8->V[vy];
                    break;
                case 0x2:
                    chip8->V[vx] &= chip8->V[vy];
                    break;
                case 0x3:
                    chip8->V[vx] ^= chip8->V[vy];
                    break;
                case 0x4:
                    sum = chip8->V[vx] + chip8->V[vy];
                    chip8->V[vx] = sum & 0xFF;
                    chip8->V[0xF] = (sum > 0xFF) ? 1 : 0;
                    break;
                case 0x5:
                    chip8->V[0xF] = (chip8->V[vx] >= chip8->V[vy]);
                    chip8->V[vx] -= chip8->V[vy];
                    break;
                case 0x6:
                    chip8->V[0xF] = chip8->V[vy] & 0x1;
                    chip8->V[vx] = chip8->V[vy] >> 1;
                    break;
                case 0x7:
                    chip8->V[0xF] = (chip8->V[vy] >= chip8->V[vx]);
                    chip8->V[vx] = chip8->V[vy] - chip8->V[vx];
                    break;
                case 0xE:
                    chip8->V[0xF] = (chip8->V[vy] >> 7) & 0x1;
                    chip8->V[vx] = chip8->V[vy] << 1;
                    break;
                default:
                    break;
            }
            break;
        case 0x9:
            chip8->pc += ((chip8->V[vx] != chip8->V[vy]) ? 2 : 0);
            break;
        case 0xA:
            chip8->I = opcode & 0xFFF;
            break;
        case 0xB:
            chip8->pc = chip8->V[0x0] + (opcode & 0xFFF);
            break;
        case 0xC:
            chip8->V[vx] = (rand() % 0x100) & lo_byte;
            break;
        case 0xD:
            chip8->V[0xF] = (display_sprite(chip8, ren, chip8->V[vx], chip8->V[vy], chip8->I, lsnibble)) & 0x1;
            break;
        case 0xE:
            switch (lo_byte) {
                case 0x9E:
                    chip8->pc += key_status(chip8->V[vx]) ? 2 : 0;
                    break;
                case 0xA1:
                    chip8->pc += !key_status(chip8->V[vx]) ? 2 : 0;
                    break;
            }
        case 0xF:
            switch (lo_byte) {
                case 0x07:
                    chip8->V[vx] = chip8->delay_timer;
                    break;
                case 0x0A:
                    if (key_ack) {
                        key_ack = 0;
                        chip8->V[vx] = pressed_key;
                    } else {
                        if (!key_syn)
                            key_syn = 1;
                        chip8->pc -= 2;
                    }
                    break;
                case 0x15:
                    chip8->delay_timer = chip8->V[vx];
                    break;
                case 0x18:
                    chip8->sound_timer = chip8->V[vx];
                    break;
                case 0x1E:
                    chip8->I += chip8->V[vx];
                    break;
                case 0x29:
                    chip8->I = (chip8->V[vx])*5 + MEM_FONT_START;
                    break;
                case 0x33: {
                    uint8_t bcd[3];
                    bin_to_bcd(chip8->V[vx], bcd);
                    memcpy(chip8->mem + chip8->I, bcd, 3);
                    break;
                }
                case 0x55:
                    memcpy(chip8->mem + chip8->I, chip8->V, vx+1);
                    chip8->I += vx+1;
                    break;
                case 0x65:
                    memcpy(chip8->V, chip8->mem + chip8->I, vx+1);
                    chip8->I += vx+1;
                    break;
            }
            break;
    }
}

