#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <SDL2/SDL.h>

#include "../include/chip8.h"
#include "../include/pixel.h"

uint8_t screen_changed = 0;

void clear_display(SDL_Renderer *ren) {
    memset(pixels, 0, TOTAL_PIXEL_BYTES);
    SDL_SetRenderDrawColor(ren, CLEAR_COLOR);
    SDL_RenderClear(ren);
    screen_changed = 1;
}

void set_pixel(SDL_Renderer *ren, uint16_t x, uint16_t y, uint8_t val) {
    if (x > NATIVE_WIDTH-1 || y > NATIVE_HEIGHT-1) {
        fprintf(stderr, "(x,y) must be in draw dimensions\n");
        return;
    }

    uint16_t pixel_byte_idx = (NATIVE_WIDTH*y + x) / 8;
    uint8_t pixel_offset = (NATIVE_WIDTH*y + x) % 8;

    SDL_Rect rect;
    rect.x = x*SCALE_FACTOR;
    rect.y = y*SCALE_FACTOR;
    rect.w = SCALE_FACTOR;
    rect.h = SCALE_FACTOR;
    if (val) {
        pixels[pixel_byte_idx] |= (1 << pixel_offset);
        SDL_SetRenderDrawColor(ren, FILL_COLOR);
    } else {
        pixels[pixel_byte_idx] &= ~(0x1 << pixel_offset);
        SDL_SetRenderDrawColor(ren, CLEAR_COLOR);
    }

    SDL_RenderFillRect(ren, &rect);

    screen_changed = 1;
}

uint8_t get_pixel(uint16_t x, uint16_t y) {
    uint16_t pixel_byte_idx = (NATIVE_WIDTH*y + x) / 8;
    uint8_t pixel_offset = (NATIVE_WIDTH*y + x) % 8;
    return (pixels[pixel_byte_idx] >> pixel_offset) & 0x1;
}

uint8_t need_screen_update() {
    return screen_changed;
}

void screen_update_done() {
    screen_changed = 0;
}

// 1 if any pixels become unset, 0 if success
uint8_t display_sprite(chip8_t* chip8, SDL_Renderer *ren, uint16_t x, uint16_t y, uint16_t addr, uint8_t height) {
    uint8_t ret = 0;
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < 8; j++) {
            uint16_t wrap_x = (x + j) % NATIVE_WIDTH;
            uint16_t wrap_y = (y + i) % NATIVE_HEIGHT;

            uint8_t pixel = get_pixel(wrap_x, wrap_y);
            uint8_t sprite_val = (((chip8->mem[addr+i]) >> (7-j)) & 0x1);
            uint8_t new_val = pixel ^ sprite_val;
            if (pixel && sprite_val)
                ret = 1;

            set_pixel(ren, wrap_x, wrap_y, new_val);
        }
    }
    return ret;
}
