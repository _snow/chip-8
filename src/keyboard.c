#include <ctype.h>
#include <stdio.h>
#include <stdint.h>

#include <SDL2/SDL.h>

#include "../include/keyboard.h"

const uint8_t key_map_keycodes[16] = {'x', '1', '2', '3', 'q', 'w', 'e', 'a', 's', 'd', 'z', 'c', '4', 'r', 'f', 'v'};
uint8_t key_map[16] = {0};

uint8_t key_syn = 0;
uint8_t key_ack = 0;
uint8_t pressed_key = 0;

void key_down_handler(SDL_KeyboardEvent e) {
    unsigned char key = tolower(e.keysym.sym);
    for (int i = 0; i < 16; i++) {
        if (key_map_keycodes[i] == key) {
            key_map[i] = 1;
            if (key_syn) {
                pressed_key = i;
                key_ack = 1;
                key_syn = 0;
            }
            break;
        }
    }
}

void key_up_handler(SDL_KeyboardEvent e) {
    unsigned char key = tolower(e.keysym.sym);
    for (int i = 0; i < 16; i++) {
        if (key_map_keycodes[i] == key) {
            key_map[i] = 0;
            break;
        }
    }
}

uint8_t key_status(unsigned char key) {
   return key_map[key];
} 
